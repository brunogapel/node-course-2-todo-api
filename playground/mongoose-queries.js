const {ObjectID} = require('mongodb');

const {mongoose} = require('./../server/db/mongoose');
const {Todo} = require('./../server/models/todo');
const {User} = require('./../server/models/user');

var todoId = '5ac29143e3aedb3fd4bc87d0';

if (!ObjectID.isValid(todoId)) {
  console.log('ID not valid');
}

Todo.find({
  _id: todoId
}).then((todos) => {
  console.log('Todos ', todos);
});

Todo.findOne({
  _id: todoId
}).then((todo) => {
  console.log('Todo: ', todo);
});

Todo.findById(todoId)
  .then((todo) => {
    if (!todo) {
      return console.log('Todo Id not found');
    }
    console.log('Todo by Id: ', todo);
  })
  .catch((e) => {
    console.log(e);
  });

var userId = '5ac29ef77d65f70a288cc491';

User.findById(userId)
  .then((user) => {
    if (!user) {
      return console.log('User Id not found');
    }
    console.log('User by Id:', JSON.stringify(user, undefined, 2));
  })
  .catch((e) => {
    console.log(e);
  });
