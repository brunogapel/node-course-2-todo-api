const {ObjectID} = require('mongodb');

const {mongoose} = require('./../server/db/mongoose');
const {Todo} = require('./../server/models/todo');
const {User} = require('./../server/models/user');

// // Empty the Todo collection
Todo.remove({})
  .then((result) => console.log(result));

Todo.findOneAndRemove({_id: '5ac3aab3cffeed2ccc9f0bd1'})
  .then((todo) => {
    console.log(JSON.stringify(todo, undefined, 2));
  });

Todo.findByIdAndRemove('5ac3aab9cffeed2ccc9f0bd2')
  .then((todo) => {
    console.log(JSON.stringify(todo, undefined, 2));
  });
